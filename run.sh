#!/usr/bin/env bash

# variables
DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# set some env variables
export NVM_DIR=~/.nvm > /dev/null 2>&1;
source ~/.nvm/nvm.sh > /dev/null 2>&1;

# run node script
nvm use v0.10.41 > /dev/null 2>&1
node $DIRECTORY/scripts/index.js
nvm use system > /dev/null 2>&1
