// imports
var Request = require( 'request' );


// class AlbumFetcher
module.exports = function AlbumFetcher ( uri, callback ) {
    // main variables
    this.uri = uri;
    this.callback = callback;

    this.fetchUri = function ( uri, callback ) {
        var splittedUri = uri.split( ':' );

        if ( splittedUri[ 1 ] !== 'album' ) {
            callback( new Error( 'Wrong album uri' ) );
        } else {
            var url = 'https://api.spotify.com/v1/albums/' + splittedUri[ 2 ];

            Request( url, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    callback( undefined, JSON.parse( response.body ) );
                } else {
                    callback( error, undefined );
                }
            });
        }
    };

    this.fetchUri( this.uri, this.callback );
};
