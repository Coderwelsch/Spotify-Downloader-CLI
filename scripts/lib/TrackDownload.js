// modules
const FileSystem = require( 'fs' ),
      Path = require( 'path' ),
      Mkdirp = require( 'mkdirp' ),
      FFMetaData = require( 'ffmetadata' );


// class track processing
module.exports = TrackDownload = function ( spotifyInstance, trackId, directory, callback ) {
    var self = this;

    // properties
    this.spotifyInstance = spotifyInstance;
    this.trackId = trackId;
    this.directory = directory;
    this.track = null;
    this.filepath = null;

    // methods
    this.getTrack = function () {
        try {
            spotifyInstance.get( trackId, function ( error, track ) {
                if ( error && typeof self.callback === 'function' ) {
                    callback( error, track );

                    return false;
                }

                self.track = track;
                self.filepath = self.createOutputDirectories();

                if ( typeof self.filepath === 'string' ) {
                    self.downloadTrack();
                } else if ( typeof callback === 'function' ) {
                    callback( new Error( 'file exists' ), track );

                    return false;
                }
            } );
        } catch ( error ) {
            callback( error );
        }
    };

    this.createOutputDirectories = function () {
        var dir = Path.resolve( this.directory ),
            artistname = self.track.artist ? self.track.artist[ 0 ].name.replace( /\//g, ' - ' ) : 'Unbenannt',
            trackname = self.track.name.replace( /\//g, ' - ' ),
            filepath = dir + '/' + artistname + ' - ' + trackname + '.mp3';

        if ( FileSystem.existsSync( filepath ) ) {
            var statistics = FileSystem.statSync( filepath );

            if ( statistics.size !== 0 ) {
                return true;
            }
        } else {
            Mkdirp.sync( dir );
        }

        return filepath;
    };

    this.downloadTrack = function () {
        var self = this,
            writeStream = FileSystem.createWriteStream( this.filepath ),
            playedTrack = self.track.play( function ( error ) {
                if ( typeof callback === 'function' ) {
                    callback( new Error( 'not playable' ), self.track );
                }

                FileSystem.unlinkSync( self.filepath );

                return;
            } );

        playedTrack
            .pipe( writeStream )
            .on( 'error', function ( error ) {
                console.log( 'ERROR', error );
                if ( typeof callback === 'function' ) {
                    callback( error );
                }

                return false;
            } )
            .on( 'finish', function () {
                self.writeMetaData( function () {
                    callback( undefined, self.track );
                } );
            } );
    };

    this.writeMetaData = function ( callback ) {
        FFMetaData.write( this.filepath, {
            artist: this.track.artist ? this.track.artist[ 0 ].name : 'Unbekannt',
			album: this.track.album.name || 'Unbekannt',
			title: this.track.name || 'Unbekannt',
			date: this.track.album.date.year || 'Unbekannt',
			track: this.track.number || 'Unbekannt'
        }, callback );
    };

    // init
    this.getTrack();
};
