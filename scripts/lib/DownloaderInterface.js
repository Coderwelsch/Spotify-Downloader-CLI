// modules
const Extend = require( 'extend' ),
      SpotifyWeb = require( 'spotify-web-cw' ),
      AlbumFetcher = require( './AlbumFetcher.js' ),
      TrackDownload = require( './TrackDownload.js' );


// class DownloaderInterface
module.exports = function DownloaderInterface ( settings ) {
    var self = this;

    this.settings = {
        username: '',
        password: '',
        playlists: '',
        directory: '',

        callbacks: {
            login: function ( error, instance ) {
                // console.log( error, instance );
            }
        }
    };

    // extend variables
    Extend( true, this.settings, settings );

    // properties
    this.callbacks = this.settings.callbacks;
    this.SpotifyWeb = null;

    if ( this.settings.directory.lastIndexOf( '/' ) !== this.settings.directory.length ) {
        this.settings.directory += '/';
    }


    // methods
    this.login = function () {
        var self = this;

        SpotifyWeb.login( this.settings.username, this.settings.password, function ( error, spotifyWebInstance ) {
            self.SpotifyWeb = spotifyWebInstance;

            if ( typeof self.callbacks.login === 'function' ) {
                self.callbacks.login( error, self.SpotifyWeb );
            }
        } );
    };

    this.getPlaylist = function ( playlistUri, callback ) {
        this.SpotifyWeb.playlist( playlistUri, function ( error, playlistData ) {
            if ( typeof callback === 'function' ) {
                callback( error, playlistData );
            }
        } );
    };

    this.getAlbum = function ( albumUri, callback ) {
        new AlbumFetcher( albumUri, callback );
    };

    this.downloadTracks = function ( playlistData, trackProcessedCallback, finishCallback ) {
        var processedItems,
            playlistName;

        if ( playlistData.contents ) {
            processedItems = playlistData.contents.items.slice();
            playlistName = playlistData.attributes.name.replace(/:/gi, '-');
        } else {
            processedItems = playlistData.tracks.items.slice();
            playlistName = playlistData.name.replace(/:/gi, '-');
        }

        function download ( track ) {
            new TrackDownload( self.SpotifyWeb, track.uri, self.settings.directory + playlistName, function ( error, trackData ) {
                if ( processedItems.length ) {
                    if ( typeof trackProcessedCallback === 'function' ) {
                        trackProcessedCallback( error, trackData );
                    }

                    download( processedItems.shift() );
                } else {
                    if ( typeof finishCallback === 'function' ) {
                        trackProcessedCallback( error, trackData );
                        finishCallback( trackData );
                    }
                }
            } );
        }

        download( processedItems.shift() );
    };


    // init
    this.login();
};
