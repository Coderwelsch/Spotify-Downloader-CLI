// very important dependency
if ( process.version !== 'v0.10.41' ) {
    console.log( 'Die Node Version muss v0.10.41 sein. Ihre Version ist jedoch %s', process.version );
    console.log( 'Tip: Benutzen Sie NVM um zwischen Node Versionen zu wechseln.' );
    process.exit( 1 );
}


// modules
const FileSystem = require( 'fs' ),
      Prompt = require( 'prompt' ),
      Colors = require( 'colors' ),
      Spawn = require( 'child_process' ).spawn,
      Downloader = require( './lib/DownloaderInterface.js' );


// paths
var dirPath = '',
    spdPath = '',
    userLoginDataPath = '';


// commands
var spdCommand = '',
    spawnSpd = null;


// TODO: SHOW ME YOUR TITS
// settings variables
var userPromptSettings = {
        properties: {
            username: {
                description: 'Geben Sie bitte Ihren Spotify Usernamen ein',
                required: true
            },
            password: {
                description: 'Geben Sie bitte Ihr Spotify Passwort ein',
                hidden: true,
                replace: '*',
                required: true
            },
            playlists: {
                description: 'URI hinzufügen (Beenden mit CTRL-C)',
                type: 'array',
                minItems: 1,
                required: true
            }
        }
    };


// data variables
var userInputs = {},
    loadedUserData = {},
    playlists = [];


// additional functions
function initVariables () {
    if ( __dirname.lastIndexOf( '/' ) === __dirname.length - 1 ) {
    	dirPath = __dirname;
    } else {
        dirPath = __dirname + '/';
    }

    spdPath = dirPath + '/node_modules/spotify-playlist-downloader/main.js';
    userLoginDataPath = dirPath + '/.userdata';
}

function logWelcomeText () {
    console.log( '**********************************************************'.green );
    console.log( '*                   SPOTIFY-DOWNLOADER                   *'.green );
    console.log( '**********************************************************'.green );
}

function logWarning () {
    console.log( 'Info: Die heruntergeladenen Musiktitel sind Privatkopien.' );
    console.log( 'Eventuell können Sie gegen die Richtlinien und AGBs von' );
    console.log( 'Spotify verstoßen.' );
}

function checkForUserData () {
    FileSystem.exists( userLoginDataPath, function ( exists ) {
        if ( exists ) {
            FileSystem.readFile( userLoginDataPath, 'utf8', function ( error, data ) {
                if ( error === null ) {
                    try {
                        loadedUserData = JSON.parse( data );

                        if ( loadedUserData.username !== '' ) {
                            delete userPromptSettings.properties.username;
                        }

                        if ( loadedUserData.password !== '' ) {
                            delete userPromptSettings.properties.password;
                        }

                        console.log( 'Ihre Logindaten wurden aus der .userdata Datei geladen.'.grey );
                    } catch ( parseError ) {
                        console.log( 'Warnung: Die .userdata Datei ist korrupt.'.yellow );
                    }
                }

                getUserCredentials();
            } );
        }
    } );
}

function getUserCredentials () {
    console.log( 'Geben Sie nun die Playlist / Alben URIs von Spotify an.' );
    console.log( 'Zum Beenden der URI Eingaben CTRL-C drücken.' );

	Prompt.start();
    Prompt.get( userPromptSettings, function ( error, result ) {
        if ( error === null ) {
        	userInputs = result;

            if ( loadedUserData.username ) {
            	userInputs.username = loadedUserData.username;
            }

            if ( loadedUserData.password ) {
                userInputs.password = loadedUserData.password;
            }

            if ( userInputs.playlists.length === 1 ) {
                console.log( 'Ein(e) Playlist / Album wird geladen'.grey );
            } else {
                console.log( '%s Playlists / Alben werden geladen'.grey, userInputs.playlists.length );
            }

            initSpotifyDownloader();
        } else {
            console.log( 'Ihre Eingaben waren nicht korrekt!'.red );
            process.exit( 1 );
        }
    } );
}

function getUserHomeDir () {
    return process.env.HOME;
}

function initSpotifyDownloader () {
    playlists = userInputs.playlists.slice();

    var spotify = null,
        downloader = new Downloader( {
            username: userInputs.username,
            password: userInputs.password,
            directory: getUserHomeDir() + '/Desktop/Spotify-Downloads/',

            callbacks: {
                login: function ( error, spotifyInstance ) {
                    if ( error ) {
                        console.log( 'Ihre Spotify Anmeldedaten sind nicht korrekt. Bitte versuchen'.red );
                        console.log( 'Sie es erneut.'.red );
                        process.exit( 1 );
                    }

                    spotify = spotifyInstance;
                    downloadPlaylist();
                }
            }
        } );

    function downloadPlaylist () {
        if ( playlists.length ) {
            var playlist = playlists.shift(),
                splittedPlaylistUri = playlist.split( ':' );

            if ( splittedPlaylistUri[ 1 ] === 'album' ) {
                downloader.getAlbum( playlist, function ( error, playlistData ) {
                    if ( error ) {
                        console.log( 'Das Album mit der URI "%s" konnte nicht gefunden werden.'.red, playlist );

                        downloadPlaylist();

                        return false;
                    }

                    console.log( 'Lade "%s" Album herunter...'.grey, playlistData.attributes ? playlistData.attributes.name : playlistData.name );

                    downloader.downloadTracks( playlistData, function ( error, trackData ) {
                        var currentTrack = '';

                        if ( trackData ) {
                            currentTrack = trackData.album.name + ' - ' + trackData.name;

                            if ( error && error.message === 'file exists' ) {
                                console.log( '~ Der Track "%s" existiert bereits'.yellow, currentTrack );
                            } else if ( error && error.message === 'not playable' ) {
                                console.log( '~ Der Track "%s" ist in deinem Land nicht verfügbar'.red, currentTrack );
                            } else if ( error ) {
                                console.log( 'x Der Track "%s" konnte nicht geladen werden. Fehler: %s'.red, currentTrack, error.message );
                            } else {
                                console.log( '√ Der Track "%s" wurde geladen.'.green, currentTrack );
                            }
                        }

                    }, function () {
                        downloadPlaylist();
                    } );
                } );
            } else {
                downloader.getPlaylist( playlist, function ( error, playlistData ) {
                    if ( error ) {
                        console.log( 'Die Playlist mit der URI "%s" konnte nicht gefunden werden.'.red, playlist );

                        downloadPlaylist();

                        return false;
                    }

                    console.log( 'Lade "%s" Playlist herunter...'.grey, playlistData.attributes.name );

                    downloader.downloadTracks( playlistData, function ( error, trackData ) {
                        var currentTrack = '';

                        if ( trackData ) {
                            currentTrack = trackData.album.name + ' - ' + trackData.name;

                            if ( error && error.message === 'file exists' ) {
                                console.log( '~ Der Track "%s" existiert bereits'.yellow, currentTrack );
                            } else if ( error && error.message === 'not playable' ) {
                                console.log( '~ Der Track "%s" ist in deinem Land nicht verfügbar'.red, currentTrack );
                            } else if ( error ) {
                                console.log( 'x Der Track "%s" konnte nicht geladen werden. Fehler: %s'.red, currentTrack, error.message );
                            } else {
                                console.log( '√ Der Track "%s" wurde geladen.'.green, currentTrack );
                            }
                        }

                    }, function () {
                        downloadPlaylist();
                    } );
                } );
            }
        } else {
            console.log( 'Alle Playlists / Alben wurden heruntergeladen.' );

            process.exit( 0 );
        }
    }
}

// main functions
function init () {
    initVariables();
    logWelcomeText();
    logWarning();
    checkForUserData();
}


// start app
init();
