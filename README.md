# Spotify Downloader ( CLI )
This is a tiny script to download real your spotify songs as a private copy.

**Info**: this script was tested only on OS X El Capitan ( 10.11.3 ). Currently only german language is supported.

![Screenshot](https://i.imgur.com/0ilaTQS.png)

## Lazy Mode
An easier way to run the script ( without struggling with nvm ) just run the ./run.sh file. It will ( re ) sets the node version automatically.

## Install & Dependencies
To install the Spotify Downloader you need the following packages:

    npm
    nvm

To install the npm script dependencies you have to use the node version v0.10.41:

    nvm install v0.10.41
    nvm use v0.10.41

After that, just run:

    npm install scripts/

## Usage
To run the script just type:

    nvm use v0.10.41 # set node version
    node scripts/index.js # run the script
    nvm use system # reset to default node version

### Output directory
Currently the playlist data output is set to the users _Desktop -> Spotify-Downloads_. This will be optional in the next release.

### Save Spotify Login Data
To save your user credentials just open the hidden _scripts/.userdata_ file and write your username and password in it.

### Contributors
Thanks to [TooTallNate](https://github.com/TooTallNate/node-spotify-web) and his spotify-web module. I have to use a modified version of this package to prevent the _Track is not available in your country_ error
